﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="analyse.aspx.cs" Inherits="Chitragupt_analyse" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
    <title>Weight Analyzer</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link href="../css/quiz.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->  
</head>
<body>
    <form id="form1" runat="server">
    <header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                   <%-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>--%>
                    <a class="navbar-brand" href="../Default.aspx"><h1>Food Nutrition</h1></a>
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                   <ul class="nav navbar-nav">
						    <li class=><a  href="../Default.aspx">Home</a></li><li class=><a  href="../nutriinfo.aspx">Nuitrition Information</a></li><li class=><a  href="../contactus.aspx">Contact Us</a></li><li class=><a  href="Default.aspx">Logout</a></li></ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->

    <section id="login" class="container">
        <center><h1>Weight Analyzer</h1></center>
        <div class="row contact-wrap wow zoomIn"> 
			<div class="status alert alert-success" style="display: none"></div>
				<div class="col-sm-4 col-sm-offset-4">
										<%--<form id="login_form" class="login_form shadow_box" method="post">--%>
                                        <div class="login_form shadow_box">
						<div class="form-group">
							<label>Enter Your Weight</label>
                            <asp:TextBox runat="server" ID="TextBox1" class="form-control" placeholder="Enter Weight"></asp:TextBox>
							<%--<input type="text" id="id" name="email" class="form-control" placeholder="Email Id / user id" required="required">--%>
						</div>
                        <div class="form-group">
							<div class="col-md-8">
      <br />
      <br />
      
      <br />
      <br />
      <br />
      Fat : 
      <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
      <br />
       calories: 
      <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
      <br />
       Iron : 
      <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
      <br />
       Sugar : 
      <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
      <br />
       Potassium : 
      <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
      <br />
      <br />

  </div>
                            </div>
						
							<%--<button type="submit" name="login" class="btn btn-primary ">Login</button>--%>
                            <asp:Button runat="server" ID="signup" name="login" class="btn btn-primary " 
                                Text="Submit" onclick="signup_Click" />
						</div><br>
						<%--<a href="forgotPassword.html" class="forgot_password pull-right">Forgot password?</a>
						<a href="signup.html" id="new_user" class="btn btn-primary">New User?</a><br>--%>
                        </div>
					<%--</form>--%> 
				</div>
		</div><!--/.row-->
    </section>



     <footer id="footer" class="midnight-blue" style="margin-top:50px; height:100px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a target="_blank" href="#" style="margin-top:50px;">food nutrition @ 2K16</a>
                </div>
                <div class="col-sm-6">
                   <%-- <ul class="pull-right">
                        <li><a href="index-2.html">Home</a></li>
                        <li><a href="login.html">Trial test</a></li>
                        <li><a href="login.html">Premium test</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>--%>
                   
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
    </form>
</body>
</html>
