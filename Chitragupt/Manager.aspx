﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Manager.aspx.cs" Inherits="Chitragupt_Manager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
    <title>Registration</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link href="../css/quiz.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
</head>
<body>
   <form id="Form1" runat="server">
<header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                   <%-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>--%>
                    <a class="navbar-brand" href="../Default.aspx"><h1>Food Nutrition</h1></a>
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                   <ul class="nav navbar-nav">
						    <li class=><a  href="../Default.aspx">Home</a></li><li class=><a  href="../nutriinfo.aspx">Nuitrition Information</a></li><li class=><a  href="Default.aspx">Login</a></li><li class=><a  href="../contactus.aspx">Contact Us</a></li></ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->
    
    <section id="login" class="container">
        <center><h1>Registration</h1></center>
        <div class="row contact-wrap wow zoomIn"> 
			<div class="status alert alert-success" style="display: none"></div>
				<div class="col-sm-4 col-sm-offset-4">
										
                                        <div class="login_form shadow_box">
						<div class="form-group">
							<label>First Name</label>
                            <asp:TextBox runat="server" ID="fname" class="form-control" placeholder="First Name"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>Last Name</label>
                            <asp:TextBox runat="server" ID="lname" class="form-control" placeholder="Last Name"></asp:TextBox>
							
						</div>
						<div class="form-group">
							<label>Email Id</label>
                             <asp:TextBox runat="server" ID="email" class="form-control" placeholder="Email Id" TextMode="Email"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>Password</label>
                             <asp:TextBox runat="server" ID="pass" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>ConfirmPass</label>
                             <asp:TextBox runat="server" ID="conpass" class="form-control" placeholder="Confirm Password" TextMode="Password"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>Address</label>
                             <asp:TextBox runat="server" ID="address" class="form-control" placeholder="Address" TextMode="MultiLine"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>Mobile Number</label>
                             <asp:TextBox runat="server" ID="Mobile" class="form-control" placeholder=" Mobile" TextMode="Number" MaxLength="10"></asp:TextBox>
							
						</div>
                        <div class="form-group">
							<label>Avtar</label>
                            <asp:FileUpload ID="avtars" runat="server"></asp:FileUpload>
						</div>
						<div class="form-group">
							
                            <asp:Button runat="server" ID="signup" name="login" class="btn btn-primary " 
                                Text="Submit" onclick="signup_Click" />
						</div><br />
						
                        </div>
				 
				</div>
		</div><!--/.row-->
    </section>
   	    <footer id="footer" class="midnight-blue" style="margin-top:50px; height:100px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a target="_blank" href="#" style="margin-top:50px;">food nutrition @ 2K16</a>
                </div>
                <div class="col-sm-6">
                   <%-- <ul class="pull-right">
                        <li><a href="index-2.html">Home</a></li>
                        <li><a href="login.html">Trial test</a></li>
                        <li><a href="login.html">Premium test</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>--%>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="pass" ControlToValidate="conpass" Display="Dynamic" 
                        ErrorMessage="Password Does not match..!!" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/main.js"></script>
    <script src="../js/wow.min.js"></script>
    </form>
</body>
</html>
