﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Chitragupt_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
    <title>Login</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link href="../css/quiz.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
</head>
<body>
    <form id="Form1" runat="server">
<header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                 
                    <a class="navbar-brand" href="#"><h1>Food nutrition</h1></a>
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                    
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->
    
    <section id="login" class="container">
        <center><h1>Login</h1></center>
        <div class="row contact-wrap wow zoomIn"> 
			<div class="status alert alert-success" style="display: none"></div>
				<div class="col-sm-4 col-sm-offset-4">
										<%--<form id="login_form" class="login_form shadow_box" method="post">--%>
                                        <div class="login_form shadow_box">
						<div class="form-group">
							<label>User id</label>
                            <asp:TextBox runat="server" ID="id" class="form-control" placeholder="Email Id / user id"></asp:TextBox>
							<%--<input type="text" id="id" name="email" class="form-control" placeholder="Email Id / user id" required="required">--%>
						</div>
						<div class="form-group">
							<label>Password</label>
                             <asp:TextBox runat="server" ID="password" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
							<%--<input type="password" id="password" name="password" placeholder="Password" class="form-control" required="required">--%>
						</div>
						<div class="form-group">
							<%--<button type="submit" name="login" class="btn btn-primary ">Login</button>--%>
                            <asp:Button runat="server" ID="signup" name="login" class="btn btn-primary " 
                                Text="Login" onclick="signup_Click" />
						</div><br>
						<%--<a href="forgotPassword.html" class="forgot_password pull-right">Forgot password?</a>
						<a href="signup.html" id="new_user" class="btn btn-primary">New User?</a><br>--%>
                        </div>
					<%--</form>--%> 
				</div>
		</div><!--/.row-->
    </section>
   	    <footer id="footer" class="midnight-blue" style="margin-top:50px; height:100px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a target="_blank" href="#" style="margin-top:50px;">Food nutrition @ 2K16</a>
                </div>
                <div class="col-sm-6">
                 
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/main.js"></script>
    <script src="../js/wow.min.js"></script>
    </form>
</body>
</html>
