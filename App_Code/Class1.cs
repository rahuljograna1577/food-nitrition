﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Class1
/// </summary>
public class Class1
{
    SqlConnection conn = new SqlConnection();
	public Class1()
	{
        conn.ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True;User Instance=True";
	}
    public DataSet calldata(string str)
    {
        SqlDataAdapter adp = new SqlDataAdapter(str,conn);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    public void savedata(string str)
    {
        SqlCommand cmd = new SqlCommand(str,conn);
        conn.Open();
        cmd.ExecuteNonQuery();
        conn.Close();
    }
}