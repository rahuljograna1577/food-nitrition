﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Gallry.aspx.cs" Inherits="Gallry" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
    <title>Gallary | Gopalak-Chhatralay</title>
    <meta name="description" content="Gopalak-Chhatralay">
    <meta name="author" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/quiz.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
</head>
<body>
   <form id="Form1" runat="server">
<header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                 
                    <a class="navbar-brand" href="#"><h1>Gopalak-Chhatralay</h1></a>
                </div>
                
                
                <div class="collapse navbar-collapse navbar-right">
                     <ul class="nav navbar-nav">
						    <li class=><a  href="Default.aspx">Home</a></li><li class=><a  href="Students.aspx">Student Information</a></li><li class=><a  href="Gallry.aspx">Gallary</a></li><li class=><a  href='#'>Activity</a></li><li class=><a  href='#'>Trust Info</a></li></ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->
    
    <section id="login" class="container">
        <center><h1>Image Gallary</h1></center>
        <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image1" runat="server" ImageUrl="~/images/slider/hostel.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
        <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image2" runat="server" ImageUrl="~/images/slider/DSC06018 (2).JPG" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
        <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image3" runat="server" ImageUrl="~/images/slider/2.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
        <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image4" runat="server" ImageUrl="~/images/slider/3.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image5" runat="server" ImageUrl="~/images/slider/4.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image6" runat="server" ImageUrl="~/images/slider/5.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image7" runat="server" ImageUrl="~/images/slider/6.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image8" runat="server" ImageUrl="~/images/slider/7.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image9" runat="server" ImageUrl="~/images/slider/8.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image10" runat="server" ImageUrl="~/images/slider/9.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image11" runat="server" ImageUrl="~/images/slider/10.jpg" Height="600" Width="1200"></asp:Image>
        </div>
 <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image12" runat="server" ImageUrl="~/images/slider/11.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image13" runat="server" ImageUrl="~/images/slider/12.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image14" runat="server" ImageUrl="~/images/slider/13.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>
         <div class="row">
       
        <div class="col-lg-12">
<asp:Image ID="Image15" runat="server" ImageUrl="~/images/slider/14.jpg" Height="600" Width="1200"></asp:Image>
        </div>
        </div>

        </div>
       <%-- <div class="row contact-wrap wow zoomIn"> 
			<div class="status alert alert-success" style="display: none"></div>
				<div class="col-sm-3 col-sm-offset-1">
										
                                        
						<asp:Image ID="Image1" runat="server" ImageUrl="~/images/slider/bgsss.jpg" Height="500" Width="800"></asp:Image>
                        
					
				</div>
		</div>--%><!--/.row-->
    </section>
   	    <footer id="footer" class="midnight-blue" style="margin-top:50px; height:100px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a target="_blank" href="#" style="margin-top:50px;">GOPALAK-CHHTRALAY @ 2K16</a>| Developed by <a style="color:white" href="#">#RJ(RAHUL JOGRANA)</a>
                </div>
                <div class="col-sm-6">
                  <ul class="pull-right">
                    <li><a href="Chitragupt/Default.aspx">Login</a></li>
                        <li><a href="Default.aspx">Home</a></li>
                        <li><a href="Students.aspx">Student Information</a></li>
                        <li><a href="Gallry.aspx">Gallary</a></li>
                        <li><a href="">Trust Information</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/main.js"></script>
    <script src="../js/wow.min.js"></script>
    </form>
</body>
</html>
